#!/usr/bin/env python3
import os
import sys
import argparse

import numpy as np
import matplotlib.pyplot as plt
import scipy.io as sio

from sklearn.cluster import KMeans
from sklearn.metrics import confusion_matrix
from sklearn.metrics import accuracy_score

DBG = False

class predictLocations():
    def __init__(self, dataDir):
        # setup
        self.eda_folder     = './eda/'
        self.results_folder = './results/'
        self.raw_data       = {}

        # load data
        for filename in os.listdir(dataDir):
            if filename.endswith(".mat"):
                # load mat file into dict
                mat = sio.loadmat(os.path.join(dataDir, filename))

                # find label we care about
                index = None
                dkeys = list(mat.keys())
                for i, val in enumerate(dkeys):
                    if 'week' in val:
                        index = i
                        break

                if(DBG): print(dkeys)
                if(DBG): print(index)

                # store only the needed information
                if index != None:
                    week_data = mat[dkeys[index]]
                    self.raw_data[dkeys[index]] = week_data
                    if(DBG): print(week_data)


    # explore data
    def eda(self):
        if not os.path.exists(self.eda_folder):
            os.makedirs(self.eda_folder)

        # generate some plots
        self.plot_data(self.raw_data['week1'], 'week1')
        self.plot_data(self.raw_data['week2'], 'week2')
        self.plot_data(self.raw_data['week3'], 'week3')
        self.plot_data(self.raw_data['week4'], 'week4')
        self.plot_data(self.raw_data['week5'], 'week5')

        # look at the numbers
        self.stats_data(self.raw_data['week1'], 'week1')
        self.stats_data(self.raw_data['week2'], 'week2')
        self.stats_data(self.raw_data['week3'], 'week3')
        self.stats_data(self.raw_data['week4'], 'week4')
        self.stats_data(self.raw_data['week5'], 'week5')

    # data stats
    def stats_data(self, data, dname):
        # break out raw data
        time  = data[:,0]
        lat   = data[:,1]
        lon   = data[:,2]
        acc   = data[:,3]
        label = data[:,4]

        # plot the data and label some stats
        # histogram
        fig, axs = plt.subplots(1, 2, tight_layout=True)
        #
        uval = np.unique(label)
        unum = uval.shape[0]
        axs[0].set_title('label: unum: ' + str(unum))
        axs[0].hist(label, bins=unum)
        #
        uval = np.unique(acc)
        unum = uval.shape[0]
        axs[1].set_title('acc: unum: ' + str(unum))
        axs[1].hist(acc, bins=unum)

        fig.savefig(self.eda_folder + 'hist_' + dname + '.png')

        # label
        fig, ax = plt.subplots()
        ax.plot(time, label, color='green')
        ax.grid()
        ax.set_xlabel('time')
        ax.set_ylabel('label')
        ax.set_title('time min/max: '  + str(np.amin(time)) + str(np.amax(time)) +
                     'label min/max: ' + str(np.amin(label)) + str(np.amax(label))
                     )

        fig.savefig(self.eda_folder + 'label_' + dname + '.png')

        # acc
        fig, ax = plt.subplots()
        ax.plot(time, label, color='green')
        ax.grid()
        ax.set_xlabel('time')
        ax.set_ylabel('acc')
        ax.set_title('time min/max: ' + str(np.amin(time)) + str(np.amax(time)) +
                     'acc min/max: '  + str(np.amin(acc)) + str(np.amax(acc))
                    )

        fig.savefig(self.eda_folder + 'acc_' + dname + '.png')
        plt.close('all')

    # plot data
    def plot_data(self, data, dname):
        # break out raw data
        time  = data[:,0]
        lat   = data[:,1]
        lon   = data[:,2]
        acc   = data[:,3]
        label = data[:,4]

        # plot
        ax.scatter(lon,
                   lat,
                   c=time)
        ax.grid()
        ax.set_xlabel('longitude')
        ax.set_ylabel('latitude')
        ax.set_title('long vs lat by time')
        fig.savefig(self.eda_folder + 'long_lat_time_' + dname + '.png')

        # plot
        fig, ax = plt.subplots()
        ax.scatter(lon,
                   lat,
                   c=label)
        ax.grid()
        ax.set_xlabel('longitude')
        ax.set_ylabel('latitude')
        ax.set_title('long vs lat by label')
        fig.savefig(self.eda_folder + 'long_lat_label_' + dname + '.png')
        plt.close('all')

    # get features
    def get_features(self):
        # sort the weeks to be in order and concatinate into a large array
        skeys  = sorted(self.raw_data)
        for i, val in enumerate(skeys):
            if i == 0:
                atrain = self.raw_data[val]
            else:
                atrain = np.vstack((atrain, self.raw_data[val]))

        # print out some info
        print('train data size: ' + str(atrain.shape))
        if(DBG): print(atrain)

        # sort into features and labels
        self.y_train = atrain[:,-1]
        self.y_test  = 0
        self.x_train = atrain[:,1:3]
        self.x_test  = 0

        self.uclasses = np.unique(self.y_train)
        self.uclasses = np.sort(self.uclasses)

    # train
    def train(self):
        self.get_features()

        # run kmeans with a few restarts and with
        # different cluster amounts 
        self.clu = []
        for i in range(1, 10):
            cclu = KMeans(n_clusters=i, algorithm='full').fit(self.x_train)
            self.clu.append(cclu)

            print('running with k = ' + str(i) + ' clusters for ' + str(cclu.n_iter_) + ' itterations')
            print('\t' + 'inertia = ' + str(cclu.inertia_))

    # predict
    def predict(self):
        pass

    def find_elbow(self, array):

        '''
        # select cluster amount using elbow method
        # https://stackoverflow.com/questions/2018178/finding-the-best-trade-off-point-on-a-curve
        curve = inertia
        nPoints = len(curve)
        allCoord = np.vstack((range(nPoints), curve)).T
        np.array([range(nPoints), curve])
        firstPoint = allCoord[0]
        lineVec = allCoord[-1] - allCoord[0]
        lineVecNorm = lineVec / np.sqrt(np.sum(lineVec**2))
        vecFromFirst = allCoord - firstPoint
        scalarProduct = np.sum(vecFromFirst * np.matlib.repmat(lineVecNorm, nPoints, 1), axis=1)
        vecFromFirstParallel = np.outer(scalarProduct, lineVecNorm)
        vecToLine = vecFromFirst - vecFromFirstParallel
        distToLine = np.sqrt(np.sum(vecToLine ** 2, axis=1))
        idxOfBestPoint = np.argmax(distToLine)
        #idxOfWorstPoint = np.argmin(distToLine)
        #distToLine[idxOfBestPoint] = distToLine[idxOfWorstPoint]
        #idxOfBestPoint = np.argmax(distToLine)
        choosen_cluster = idxOfBestPoint + 1
        '''

        # compute the second derivitive for each point
        # and find the max
        x  = np.append(array[0], np.append(array, array[-1]))
        sd = np.zeros(x.shape[0])
        for i in range(1, x.shape[0]-1):
            sd[i] = x[i + 1] + x[i - 1] - (2*x[i])
       
        choosen_cluster = np.argmax(sd)

        return choosen_cluster

    # plot results
    def plot_results(self):
        if not os.path.exists(self.results_folder):
            os.makedirs(self.results_folder)

        # extract info from clu
        for i, val in enumerate(self.clu):
            if i == 0:
                inertia = np.array(val.inertia_)
            else:
                inertia = np.append(inertia, val.inertia_)

        # log of the loss
        inertia = np.log(inertia)

        # plot error
        fig, ax = plt.subplots()
        ax.plot(inertia, color='red')
        ax.grid()
        ax.set_xlabel('clusters')
        ax.set_ylabel('error')
        ax.set_title('Squared Error by Cluster Amount')
        fig.savefig(self.results_folder + 'clu_error.png')

        # find curve
        choosen_cluster = self.find_elbow(inertia)

        # display results
        print('correct cluster amount: ' + str(self.uclasses.shape[0]))
        print('elbow cluster amount: ' + str(choosen_cluster))

        # setup scatter
        choosen_model = self.clu[choosen_cluster-1]
        lon   = self.x_train[:,0]
        lat   = self.x_train[:,1]
        label = choosen_model.labels_

        # plot choosen clusters
        fig, ax = plt.subplots()
        ax.scatter(lon,
                   lat,
                   c=label)
        ax.grid()
        ax.set_xlabel('lon')
        ax.set_ylabel('lat')
        ax.set_title('Kmeans Clusters')
        fig.savefig(self.results_folder + 'clu.png')

        plt.close('all')


def main():
    '''
    Entrance of the program
    '''

    # parse command line arguments
    parser = argparse.ArgumentParser()
    # arguments
    parser.add_argument("dataDir", type=str, help='Input folder for data',
                        nargs='?', const=1, default='./data')
    parser.add_argument('-d', '--debug',
                        help='Debug mode - extra print statements',
                        action='store_true')
    parser.add_argument('-e', '--eda',
                        help='EDA mode - generate exploratory data analysis plots',
                        action='store_true')
    args = parser.parse_args()

    # setup debug flag
    global DBG
    if(args.debug):
        DBG = True

    # create object
    pl = predictLocations(args.dataDir)
    if(args.eda):
        print('generating EDA ...')
        pl.eda()

    # train and predict
    print('training ...')
    pl.train()

    # output results
    print('generating results ...')
    pl.plot_results()


if __name__ == '__main__':
    main()
