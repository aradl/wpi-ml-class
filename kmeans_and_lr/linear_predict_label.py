#!/usr/bin/env python3
import os
import sys
import argparse

import numpy as np
import matplotlib.pyplot as plt
import scipy.io as sio

from sklearn import svm
from sklearn.linear_model import LogisticRegression
from sklearn.linear_model import LogisticRegressionCV
from sklearn.metrics import confusion_matrix
from sklearn.metrics import accuracy_score

DBG = False

class predictLabel():
    def __init__(self, dataDir):
        # setup
        self.eda_folder = './eda/'
        self.raw_data   = {}

        # load data
        for filename in os.listdir(dataDir):
            if filename.endswith(".mat"):
                # load mat file into dict
                mat = sio.loadmat(os.path.join(dataDir, filename))

                # find label we care about
                index = None
                dkeys = list(mat.keys())
                for i, val in enumerate(dkeys):
                    if 'week' in val:
                        index = i
                        break

                if(DBG): print(dkeys)
                if(DBG): print(index)

                # store only the needed information
                if index != None:
                    week_data = mat[dkeys[index]]
                    self.raw_data[dkeys[index]] = week_data
                    if(DBG): print(week_data)


    # explore data
    def eda(self):
        if not os.path.exists(self.eda_folder):
            os.makedirs(self.eda_folder)

        # generate some plots
        self.plot_data(self.raw_data['week1'], 'week1')
        self.plot_data(self.raw_data['week2'], 'week2')
        self.plot_data(self.raw_data['week3'], 'week3')
        self.plot_data(self.raw_data['week4'], 'week4')
        self.plot_data(self.raw_data['week5'], 'week5')

        # look at the numbers
        self.stats_data(self.raw_data['week1'], 'week1')
        self.stats_data(self.raw_data['week2'], 'week2')
        self.stats_data(self.raw_data['week3'], 'week3')
        self.stats_data(self.raw_data['week4'], 'week4')
        self.stats_data(self.raw_data['week5'], 'week5')

    # data stats
    def stats_data(self, data, dname):
        # break out raw data
        time  = data[:,0]
        lat   = data[:,1]
        lon   = data[:,2]
        acc   = data[:,3]
        label = data[:,4]

        # plot the data and label some stats
        # histogram
        fig, axs = plt.subplots(1, 2, tight_layout=True)
        #
        uval = np.unique(label)
        unum = uval.shape[0]
        axs[0].set_title('label: unum: ' + str(unum))
        axs[0].hist(label, bins=unum)
        #
        uval = np.unique(acc)
        unum = uval.shape[0]
        axs[1].set_title('acc: unum: ' + str(unum))
        axs[1].hist(acc, bins=unum)

        fig.savefig(self.eda_folder + 'hist_' + dname + '.png')

        # label
        fig, ax = plt.subplots()
        ax.plot(time, label, color='green')
        ax.grid()
        ax.set_xlabel('time')
        ax.set_ylabel('label')
        ax.set_title('time min/max: '  + str(np.amin(time)) + str(np.amax(time)) +
                     'label min/max: ' + str(np.amin(label)) + str(np.amax(label))
                     )

        fig.savefig(self.eda_folder + 'label_' + dname + '.png')

        # acc
        fig, ax = plt.subplots()
        ax.plot(time, label, color='green')
        ax.grid()
        ax.set_xlabel('time')
        ax.set_ylabel('acc')
        ax.set_title('time min/max: ' + str(np.amin(time)) + str(np.amax(time)) +
                     'acc min/max: '  + str(np.amin(acc)) + str(np.amax(acc))
                    )

        fig.savefig(self.eda_folder + 'acc_' + dname + '.png')
        plt.close('all')

    # plot data
    def plot_data(self, data, dname):
        # break out raw data
        time  = data[:,0]
        lat   = data[:,1]
        lon   = data[:,2]
        acc   = data[:,3]
        label = data[:,4]

        # plot
        fig, ax = plt.subplots()
        ax.scatter(lon,
                   lat,
                   c=time)
        ax.grid()
        ax.set_xlabel('longitude')
        ax.set_ylabel('latitude')
        ax.set_title('long vs lat by time')
        fig.savefig(self.eda_folder + 'long_lat_time_' + dname + '.png')

        # plot
        fig, ax = plt.subplots()
        ax.scatter(lon,
                   lat,
                   c=label)
        ax.grid()
        ax.set_xlabel('longitude')
        ax.set_ylabel('latitude')
        ax.set_title('long vs lat by label')
        fig.savefig(self.eda_folder + 'long_lat_label_' + dname + '.png')
        plt.close('all')

    # get features
    def get_features(self):
        # sort data for training and testing
        skeys  = sorted(self.raw_data)
        for i, val in enumerate(skeys):

            # place the last dataset into test
            if i == (len(skeys) - 1):
                atest = self.raw_data[val]
            else:
                if i == 0:
                    atrain = self.raw_data[val]
                else:
                    atrain = np.vstack((atrain, self.raw_data[val]))

        # print out some info
        print('train data size: ' + str(atrain.shape))
        if(DBG): print(atrain)
        print('test data size: ' + str(atest.shape))
        if(DBG): print(atest)

        # sort into features and class
        self.y_train = atrain[:,-1]
        self.y_test  = atest[:,-1]
        self.x_train = atrain[:,0:-1]
        self.x_test  = atest[:,0:-1]
        # lets only use time for now (can't get better than 75%)
        #self.x_train = atrain[:,0].reshape(-1, 1)
        #self.x_test  = atest[:,0].reshape(-1, 1)
        #
        self.uclasses = np.unique(np.append(self.y_train, self.y_test))
        self.uclasses = np.sort(self.uclasses)

    # train
    def train(self):
        self.get_features()

        # implement logistic regression
        '''
        self.clf = LogisticRegression(C=10, penalty='l2',
                                      solver='newton-cg',
                                      multi_class='multinomial',
                                      max_iter=10000)
        self.clf.fit(self.x_train, self.y_train)
        '''
        # with cross validation
        self.clf = LogisticRegressionCV(cv=5,
                                        multi_class='multinomial',
                                        max_iter=10000)
        self.clf.fit(self.x_train, self.y_train)

    # predict
    def predict(self):
        self.y_pred = self.clf.predict(self.x_test)
        self.y_prob = self.clf.predict_proba(self.x_test)

    # plot results
    def plot_results(self):
        print('Confusion Matrix: ')
        print(confusion_matrix(self.y_test, self.y_pred))
        print('Accuracy Score: ' + str(accuracy_score(self.y_test, self.y_pred)))

        # remap label indicies to select the confidence of the choosen label
        remapped_pred = np.zeros(self.y_pred.shape[0], dtype=int)
        for i, val in enumerate(self.uclasses):
            remapped_pred[self.y_pred == val] = i

        # average the confidence
        selected_prob = self.y_prob[np.arange(remapped_pred.shape[0]),
                                        remapped_pred]

        print('Average Confidence: ' + str(np.average(selected_prob)))

def main():
    '''
    Entrance of the program
    '''

    # parse command line arguments
    parser = argparse.ArgumentParser()
    # arguments
    parser.add_argument("dataDir", type=str, help='Input folder for data',
                        nargs='?', const=1, default='./data')
    parser.add_argument('-d', '--debug',
                        help='Debug mode - extra print statements',
                        action='store_true')
    parser.add_argument('-e', '--eda',
                        help='EDA mode - generate exploratory data analysis plots',
                        action='store_true')
    args = parser.parse_args()

    # setup debug flag
    global DBG
    if(args.debug):
        DBG = True

    # create object
    pl = predictLabel(args.dataDir)
    if(args.eda):
        print('generating EDA ...')
        pl.eda()

    # train and predict
    print('training ...')
    pl.train()
    print('predicting ...')
    pl.predict()

    # output results
    print('generating results ...')
    pl.plot_results()


if __name__ == '__main__':
    main()
