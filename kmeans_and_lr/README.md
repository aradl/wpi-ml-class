## Q1) Linear Regression

To setup the data place all weekX.mat files in a directory 
and point the program to it. './data' is the default. It will automatically use
the first X-1 files to train and the Xth file to test.

To run the code use (It takes about a minute to run):
```
python3 ./linear_predict_label.py ./mat_file_dir
```

Output example:
```
./linear_predict_label.py ./data/
training ...
train data size: (1344, 5)
test data size: (336, 5)
predicting ...
generating results ...
Confusion Matrix: 
[[152   0   0   0   0]
 [  0   0   0   6   0]
 [  0   0 110   0   0]
 [  0   0   0  64   0]
 [  0   0   0   0   4]]
Accuracy Score: 0.9821428571428571
Average Confidence: 0.9790134150107306
```

## Q2) K Means
To setup the data place all weekX.mat files in a directory 
and point the program to it. './data' is the default. 
It will use all X files to train.

To run the code use:
```
python3 ./kmeans_predict_locations.py ./mat_file_dir
```

Output example:
```
./kmeans_predict_locations.py ./data/
training ...
train data size: (1680, 5)
running with k = 1 clusters for 2 itterations
	inertia = 1960.3770822859592
running with k = 2 clusters for 2 itterations
	inertia = 98.53708416096566
running with k = 3 clusters for 9 itterations
	inertia = 61.44588347308637
running with k = 4 clusters for 15 itterations
	inertia = 39.475980381802316
running with k = 5 clusters for 15 itterations
	inertia = 25.802201729930644
running with k = 6 clusters for 9 itterations
	inertia = 22.35775569333322
running with k = 7 clusters for 16 itterations
	inertia = 19.756214367398034
running with k = 8 clusters for 10 itterations
	inertia = 16.855638354261657
running with k = 9 clusters for 15 itterations
	inertia = 14.106175108583695
generating results ...
correct cluster amount: 5
elbow cluster amount: 2
```

To view the log error graph see './results/clu_error.png'
