import foolbox
import keras
import numpy as np
from keras.applications.resnet50 import ResNet50
from keras.applications.resnet50 import preprocess_input
from keras.applications.resnet50 import decode_predictions
import matplotlib.pyplot as plt

def plot_output_batch(images, labels, probs, adversarials, adv_labels, adv_probs, title):

    i_count = images.shape[0]
    index   = 1

    plt.figure(figsize=(8.5,11))
    for i in range(i_count):
        image       = images[i]
        adversarial = adversarials[i]

        plt.subplot(i_count, 3, 1 + (i * 3))
        plt.title('Original', fontsize=10)
        plt.imshow(image / 255)  # division by 255 to convert [0, 255] to [0, 1]
        plt.text(-175, 300,'{0}: {1:.2}'.format(labels[i], probs[i]))
        plt.axis('off')
        
        plt.subplot(i_count, 3, 2 + (i * 3))
        plt.title('Adversarial', fontsize=10)
        plt.imshow(adversarial[:, :, ::-1] / 255)  # ::-1 to convert BGR to RGB
        plt.text(0, 300,'{0}: {1:.2}'.format(adv_labels[i], adv_probs[i]))
        plt.axis('off')
        
        plt.subplot(i_count, 3, 3 + (i * 3))
        plt.title('Difference', fontsize=10)
        difference = adversarial[:, :, ::-1] - image
        plt.imshow(difference / abs(difference).max() * 0.2 + 0.5)
        plt.axis('off')

    plt.subplots_adjust(hspace=1.0)
    plt.savefig(title + '.png')
    

# instantiate model
keras.backend.set_learning_phase(0)
kmodel        = ResNet50(weights='imagenet')
preprocessing = (np.array([104, 116, 123]), 1)
fmodel        = foolbox.models.KerasModel(kmodel, bounds=(0, 255), preprocessing=preprocessing)

# get 10 source image and label examples
images, labels = foolbox.utils.samples(dataset='imagenet', index=3, batchsize=24)
images = images[-10:]
labels = labels[-10:]

# get label probably
probs    = np.zeros(10)
labels_w = []
for i in range(images.shape[0]):
    i_rgb           = images[i, np.newaxis, :, :, ::-1]
    preds           = kmodel.predict(preprocess_input(i_rgb.copy()))
    pred_dict       = decode_predictions(preds, top=1)
    probs[i]        = pred_dict[0][0][2]
    labels_w.append(pred_dict[0][0][1])


#
# attack types
# 
attack_handler = {'BlendedUniformNoiseAttack': foolbox.attacks.BlendedUniformNoiseAttack,
                  'ContrastReductionAttack':   foolbox.attacks.ContrastReductionAttack,
                  'FGSM':                      foolbox.attacks.FGSM,
                #  'SinglePixelAttack':         foolbox.attacks.SinglePixelAttack,
                  'SaliencyMapAttack':         foolbox.attacks.SaliencyMapAttack
                 }


# loop through attack types
for key, value in attack_handler.items():
    print('running attack: {0}'.format(key))
    attack = value(fmodel)
    advs   = np.zeros([10,224,224,3])

    adv_labels = []
    adv_probs  = np.zeros(10)

    # loop through images
    for i in range(images.shape[0]):
        print('    image number: {0}/{1}'.format(i+1, images.shape[0]))
        advs[i] = attack(images[i, :, :, ::-1], labels[i])

        adversarial_rgb = advs[i, np.newaxis, :, :, ::-1]
        preds           = kmodel.predict(preprocess_input(adversarial_rgb.copy()))
        pred_dict       = decode_predictions(preds, top=3)
        print(pred_dict) 
        adv_probs[i]    = pred_dict[0][0][2]
        adv_labels.append(pred_dict[0][0][1])
   
    print('done! generating plot ...')
    plot_output_batch(images, labels_w, probs, advs, adv_labels, adv_probs, key)

exit()

# ContrastReductionAttack
attack_h = attack_handler['ContrastReductionAttack']
attack     = attack_h(fmodel)
adv          = attack(image[:, :, ::-1], label)

plot_output(image, cr_adv, 'Contrast Reduction Attack')

# FGSM
fgsm_attack = foolbox.attacks.FGSM(fmodel)
fgsm_adv    = fgsm_attack(image[:, :, ::-1], label)

plot_output(image, fgsm_adv, '(FGSM) Gradient Sign Attack')

# SinglePixelAttack
sp_attack = foolbox.attacks.SinglePixelAttack(fmodel)
sp_adv    = sp_attack(image[:, :, ::-1], label)

plot_output(image, sp_adv, 'Single Pixel Attack')

# SaliencyMapAttack
sm_attack = foolbox.attacks.SaliencyMapAttack(fmodel)
sm_adv    = sm_attack(image[:, :, ::-1], label)

plot_output(image, sm_adv, 'Saliency Map Attack')



