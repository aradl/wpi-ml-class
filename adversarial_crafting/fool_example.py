import foolbox
import keras
import numpy as np
from keras.applications.resnet50 import ResNet50
from keras.applications.resnet50 import preprocess_input
from keras.applications.resnet50 import decode_predictions
import matplotlib.pyplot as plt
import cv2

# instantiate model
keras.backend.set_learning_phase(0)
kmodel = ResNet50(weights='imagenet')
preprocessing = (np.array([104, 116, 123]), 1)
fmodel = foolbox.models.KerasModel(kmodel, bounds=(0, 255), preprocessing=preprocessing)

# get source image and label
#image, label = foolbox.utils.imagenet_example()
images, labels = foolbox.utils.samples(dataset='imagenet', shape=(50,50), index=33, batchsize=50)
attack         = foolbox.attacks.SinglePixelAttack(fmodel)
images = images[5:]
labels = labels[5:]

#image = images[3]
#label = labels[3]
#
#res = cv2.resize(image, dsize=(50,50), interpolation=cv2.INTER_CUBIC)
#
#adversarial = attack(res[ :, :, ::-1], label)

if(True):
    # apply attack on source image
    # ::-1 reverses the color channels, because Keras ResNet50 expects BGR instead of RGB
    #adversarial = attack(image[ :, :, ::-1], label)
    import warnings
    warnings.filterwarnings("error")
    for i in range(images.shape[0]):
        print(i)
        try:
            adversarial = attack(images[i, :, :, ::-1], labels[i], max_pixels=(50*50))
        except:
            continue
    
        # if the attack fails, adversarial will be None and a warning will be printed
        if adversarial is not None:
            print('found: {0}'.format(i))
            image = images[i]
            label = labels[i]
            break

res       = cv2.resize(image, (224,224))
i_rgb     = res[np.newaxis, :, :, ::-1]
preds     = kmodel.predict(preprocess_input(i_rgb.copy()))
pred_dict = decode_predictions(preds, top=2)
print('img')
print(pred_dict)
prob      = pred_dict[0][0][2]
labelw    = pred_dict[0][0][1]

res       = cv2.resize(adversarial, (224,224))
i_rgb     = res[np.newaxis, :, :, ::-1]
preds     = kmodel.predict(preprocess_input(i_rgb.copy()))
pred_dict = decode_predictions(preds, top=2)
print('adv')
print(pred_dict)
adv_prob  = pred_dict[0][0][2]
adv_label = pred_dict[0][0][1]


title = 'SinglePixelAttack' 
i = 0
plt.subplot(1, 3, 1 + (i * 3))
plt.title('Original', fontsize=10)
plt.imshow(image / 255)  # division by 255 to convert [0, 255] to [0, 1]
plt.text(-10, 60,'{0}: {1:.2}'.format(labelw, prob))
plt.axis('off')
        
plt.subplot(1, 3, 2 + (i * 3))
plt.title('Adversarial', fontsize=10)
plt.imshow(adversarial[:, :, ::-1] / 255)  # ::-1 to convert BGR to RGB
plt.text(0, 68,'{0}: {1:.2}'.format(adv_label, adv_prob))
plt.axis('off')
        
plt.subplot(1, 3, 3 + (i * 3))
plt.title('Difference', fontsize=10)
difference = adversarial[:, :, ::-1] - image
plt.imshow(difference / abs(difference).max() * 0.2 + 0.5)
plt.axis('off')

plt.subplots_adjust(hspace=1.0)
plt.savefig(title + '.png')
'''
plt.figure()

plt.subplot(1, 3, 1)
plt.title('Original')
plt.imshow(image / 255)  # division by 255 to convert [0, 255] to [0, 1]
plt.axis('off')

plt.subplot(1, 3, 2)
plt.title('Adversarial')
plt.imshow(adversarial[:, :, ::-1] / 255)  # ::-1 to convert BGR to RGB
plt.axis('off')

plt.subplot(1, 3, 3)
plt.title('Difference')
difference = adversarial[:, :, ::-1] - image
#print(np.where(difference == 0))
print(np.where(difference == 1))
plt.imshow(difference / abs(difference).max() * 0.2 + 0.5)
plt.axis('off')

plt.show()
'''
