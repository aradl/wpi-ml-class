import math
import numpy as np
from collections import Counter
# Note: please don't add any new package, you should solve this problem using only the packages above.
#-------------------------------------------------------------------------
'''
    Part 1: Decision Tree (with Discrete Attributes) -- 40 points --
    In this problem, you will implement the decision tree method for classification problems.
    You could test the correctness of your code by typing `nosetests -v test1.py` in the terminal.
'''
        
#-----------------------------------------------
class Node:
    '''
        Decision Tree Node (with discrete attributes)
        Inputs: 
            X: the data instances in the node, a numpy matrix of shape p by n.
               Each element can be int/float/string.
               Here n is the number data instances in the node, p is the number of attributes.
            Y: the class labels, a numpy array of length n.
               Each element can be int/float/string.
            i: the index of the attribute being tested in the node, an integer scalar 
            C: the dictionary of attribute values and children nodes. 
               Each (key, value) pair represents an attribute value and its corresponding child node.
            isleaf: whether or not this node is a leaf node, a boolean scalar
            p: the label to be predicted on the node (i.e., most common label in the node).
    '''
    def __init__(self,X,Y, i=None,C=None, isleaf= False,p=None):
        self.X = X
        self.Y = Y
        self.i = i
        self.C= C
        self.isleaf = isleaf
        self.p = p

#-----------------------------------------------
class Tree(object):
    '''
        Decision Tree (with discrete attributes). 
        We are using ID3(Iterative Dichotomiser 3) algorithm. So this decision tree is also called ID3.
    '''
    #--------------------------
    @staticmethod
    def entropy(Y):
        '''
            Compute the entropy of a list of values.
            Input:
                Y: a list of values, a numpy array of int/float/string values.
            Output:
                e: the entropy of the list of values, a float scalar
            Hint: you could use collections.Counter.
        '''
        #########################################
        ## INSERT YOUR CODE HERE
        
        # find all unique lables and return the count for each one
        # u = values, c = count
        uY, cY = np.unique(Y, return_counts=True)
        pmf    = np.zeros(shape=uY.shape)
        e      = 0

        # loop through unique attributes
        for i in range(0, uY.shape[0]):
            # compute pmf - probability mass function
            pmf[i] = cY[i] / Y.shape[0]

            # compute entropy per attribute
            e += pmf[i] * np.log2(pmf[i])
        
        e *= -1

        #########################################
        return e 
    
    
            
    #--------------------------
    @staticmethod
    def conditional_entropy(Y,X):
        '''
            Compute the conditional entropy of y given x. The conditional entropy H(Y|X) means average entropy of children nodes, given attribute X. Refer to https://en.wikipedia.org/wiki/Information_gain_in_decision_trees
            Input:
                X: a list of values , a numpy array of int/float/string values. The size of the array means the number of instances/examples. X contains each instance's attribute value. 
                Y: a list of values, a numpy array of int/float/string values. Y contains each instance's corresponding target label. For example X[0]'s target label is Y[0]
            Output:
                ce: the conditional entropy of y given x, a float scalar
        '''
        #########################################
        ## INSERT YOUR CODE HERE

        # find all unique lables and return the count for each one
        # u = values, c = count
        uX, cX, iX = np.unique(X, return_counts=True, return_index=True)
        ce         = 0

        # loop through unique attributes
        for i in range(0, uX.shape[0]):
            # split off sub attribute array Ya
            Ya = Y[X == uX[i]]

            # compute weighted entropy
            w = Ya.shape[0] / Y.shape[0]

            # compute entropy per attribute
            ce += w * Tree.entropy(Ya)

 
        #########################################
        return ce 
    
    
    
    #--------------------------
    @staticmethod
    def information_gain(Y,X):
        '''
            Compute the information gain of y after spliting over attribute x
            InfoGain(Y,X) = H(Y) - H(Y|X) 
            Input:
                X: a list of values, a numpy array of int/float/string values.
                Y: a list of values, a numpy array of int/float/string values.
            Output:
                g: the information gain of y after spliting over x, a float scalar
        '''
        #########################################
        ## INSERT YOUR CODE HERE
    
        g = Tree.entropy(Y) - Tree.conditional_entropy(Y, X)

 
        #########################################
        return g


    #--------------------------
    @staticmethod
    def best_attribute(X,Y):
        '''
            Find the best attribute to split the node. 
            Here we use information gain to evaluate the attributes. 
            If there is a tie in the best attributes, select the one with the smallest index.
            Input:
                X: the feature matrix, a numpy matrix of shape p by n. 
                   Each element can be int/float/string.
                   Here n is the number data instances in the node, p is the number of attributes.
                Y: the class labels, a numpy array of length n. Each element can be int/float/string.
            Output:
                i: the index of the attribute to split, an integer scalar
        '''
        #########################################
        ## INSERT YOUR CODE HERE

        ig = np.zeros(X.shape[0])

        # find foreach attribute p if there is more than one unique value
        for i in range(0, X.shape[0]):
            # split off row and find gain
            ig[i] = Tree.information_gain(Y, X[i])

        # find where gain is the largest
        i = np.argmax(ig)

 
        #########################################
        return i

        
    #--------------------------
    @staticmethod
    def split(X,Y,i):
        '''
            Split the node based upon the i-th attribute.
            (1) split the matrix X based upon the values in i-th attribute
            (2) split the labels Y based upon the values in i-th attribute
            (3) build children nodes by assigning a submatrix of X and Y to each node
            (4) build the dictionary to combine each  value in the i-th attribute with a child node.
    
            Input:
                X: the feature matrix, a numpy matrix of shape p by n.
                   Each element can be int/float/string.
                   Here n is the number data instances in the node, p is the number of attributes.
                Y: the class labels, a numpy array of length n.
                   Each element can be int/float/string.
                i: the index of the attribute to split, an integer scalar
            Output:
                C: the dictionary of attribute values and children nodes. 
                   Each (key, value) pair represents an attribute value and its corresponding child node.
        '''
        #########################################
        ## INSERT YOUR CODE HERE

        # init
        C = dict()

        # seperate Xa from X
        Xa = X[i,:]
        # cut out X[i] from X?
        #X  = np.delete(X, i, 0)
        
        # find all unique lables and return the count for each one
        # u = values, c = count
        uXa, cXa, iXa = np.unique(Xa, return_counts=True, return_index=True)

        # loop through unique attributes
        for i in range(0, uXa.shape[0]):
            value = uXa[i]

            # split off sub attribute array Ya, Xa
            Ys = Y[Xa == value]
            Xs = X[:, Xa == value]

            # create node and set to dictionary
            node     = Node(Xs,Ys, i=None,C=None, isleaf= False,p=None)
            C[value] = node


        #########################################
        return C

    #--------------------------
    @staticmethod
    def stop1(Y):
        '''
            Test condition 1 (stop splitting): whether or not all the instances have the same label. 
    
            Input:
                Y: the class labels, a numpy array of length n.
                   Each element can be int/float/string.
            Output:
                s: whether or not Conidtion 1 holds, a boolean scalar. 
                True if all labels are the same. Otherwise, false.
        '''
        #########################################
        ## INSERT YOUR CODE HERE
        

        # find unique occuences in row
        u = np.unique(Y)
        s = True

        # if there is more than 1 we must return false
        if(u.shape[0] != 1):
            s = False

        
        #########################################
        return s
    
    #--------------------------
    @staticmethod
    def stop2(X):
        '''
            Test condition 2 (stop splitting): whether or not all the instances have the same attribute values. 
            Input:
                X: the feature matrix, a numpy matrix of shape p by n.
                   Each element can be int/float/string.
                   Here n is the number data instances in the node, p is the number of attributes.
            Output:
                s: whether or not Conidtion 2 holds, a boolean scalar. 
        '''
        #########################################
        ## INSERT YOUR CODE HERE

        # find foreach attribute p if there is more than one unique value
        for i in range(0, X.shape[0]):
            # split off row
            s = Tree.stop1(X[i])

            if(not s):
                return False

        # otherwise we have only found one unique attribute per row
        s = True

 
        #########################################
        return s
    
            
    #--------------------------
    @staticmethod
    def most_common(Y):
        '''
            Get the most-common label from the list Y. 
            Input:
                Y: the class labels, a numpy array of length n.
                   Each element can be int/float/string.
                   Here n is the number data instances in the node.
            Output:
                y: the most common label, a scalar, can be int/float/string.
        '''
        #########################################
        ## INSERT YOUR CODE HERE
    
        # find all unique lables and return the count for each one
        # u = values, c = count
        uY, cY     = np.unique(Y, return_counts=True)
        maxIndex   = np.argmax(cY)

        # set output
        y = uY[maxIndex]

 
        #########################################
        return y
    
    
    
    #--------------------------
    @staticmethod
    def build_tree(t):
        '''
            Recursively build tree nodes.
            Input:
                t: a node of the decision tree, without the subtree built.
                t.X: the feature matrix, a numpy float matrix of shape n by p.
                   Each element can be int/float/string.
                    Here n is the number data instances, p is the number of attributes.
                t.Y: the class labels of the instances in the node, a numpy array of length n.
                t.C: the dictionary of attribute values and children nodes. 
                   Each (key, value) pair represents an attribute value and its corresponding child node.
        '''
        #########################################
        ## INSERT YOUR CODE HERE
    
        # Test stop conditions
        #   stop if all atrribute values are the same
        #   or if all truth labels are the same
        if(Tree.stop2(t.X) or Tree.stop1(t.Y)):
            t.p      = Tree.most_common(t.Y)
            t.isleaf = True

        else:
            # find the attribute with the most information gain and split
            if(t.C == None):
                # we are the leaf node
                aIndex = Tree.best_attribute(t.X, t.Y)
                aDict  = Tree.split(t.X, t.Y, aIndex)

                # setup node info
                t.C      = aDict
                t.i      = aIndex
                t.isleaf = False
                t.p      = Tree.most_common(t.Y)
                
                # recursive call
                Tree.build_tree(t)

            else:
                # we are the leaf node
                for key in t.C:
                    # recursive call
                    Tree.build_tree(t.C[key])
 
        #########################################
    
    
    #--------------------------
    @staticmethod
    def train(X, Y):
        '''
            Given a training set, train a decision tree. 
            Input:
                X: the feature matrix, a numpy matrix of shape p by n.
                   Each element can be int/float/string.
                   Here n is the number data instances in the training set, p is the number of attributes.
                Y: the class labels, a numpy array of length n.
                   Each element can be int/float/string.
            Output:
                t: the root of the tree.
        '''
        #########################################
        ## INSERT YOUR CODE HERE

        # create root node
        t = Node(X=X, Y=Y)

        Tree.build_tree(t)

 
        #########################################
        return t
    
    
    
    #--------------------------
    @staticmethod
    def inference(t,x):
        '''
            Given a decision tree and one data instance, infer the label of the instance recursively. 
            Input:
                t: the root of the tree.
                x: the attribute vector, a numpy vectr of shape p.
                   Each attribute value can be int/float/string.
            Output:
                y: the class labels, a numpy array of length n.
                   Each element can be int/float/string.
        '''
        #########################################
        ## INSERT YOUR CODE HERE

        # init
        valueNotFound = True

        # test stop condition
        if(t.isleaf):
            y = t.p
        else:
            # search for value in children... :|
            for value in x:
                if(value in t.C):
                    # value found
                    tn = t.C[value]
                    y  = Tree.inference(tn, x)

                    valueNotFound = False

            # if value not found
            if(valueNotFound):
                y = t.p
 
        #########################################
        return y
    
    #--------------------------
    @staticmethod
    def predict(t,X):
        '''
            Given a decision tree and a dataset, predict the labels on the dataset. 
            Input:
                t: the root of the tree.
                X: the feature matrix, a numpy matrix of shape p by n.
                   Each element can be int/float/string.
                   Here n is the number data instances in the dataset, p is the number of attributes.
            Output:
                Y: the class labels, a numpy array of length n.
                   Each element can be int/float/string.
        '''
        #########################################
        ## INSERT YOUR CODE HERE

        # init
        Y = np.zeros(X.shape[1], dtype='object')
        i = 0

        # transpose X to get a row equal to an instance
        Xt = np.transpose(X)

        # loop through instance
        for row in Xt:
            Y[i] = Tree.inference(t, row)
            i += 1


        #########################################
        return Y



    #--------------------------
    @staticmethod
    def load_dataset(filename = 'data1.csv'):
        '''
            Load dataset 1 from the CSV file: 'data1.csv'. 
            The first row of the file is the header (including the names of the attributes)
            In the remaining rows, each row represents one data instance.
            The first column of the file is the label to be predicted.
            In remaining columns, each column represents an attribute.
            Input:
                filename: the filename of the dataset, a string.
            Output:
                X: the feature matrix, a numpy matrix of shape p by n.
                   Each element can be int/float/string.
                   Here n is the number data instances in the dataset, p is the number of attributes.
                Y: the class labels, a numpy array of length n.
                   Each element can be int/float/string.
        '''
        #########################################
        ## INSERT YOUR CODE HERE
   
        ''' Alternate solution as you can preserve header names
        # take in the full csv file
        fullList = np.genfromtxt(filename, delimiter=',', names=True, dtype=None, encoding='ascii')   
    
        # seperate X and Y names
        prediction    = fullList.dtype.names[0]
        attributeList = fullList.dtype.names[1:]

        # create Y from class labels
        Y = fullList[prediction]

        X = np.empty(shape=(len(attributeList), fullList.shape[0]), dtype='object')
        i = 0
        # create X from remaining attributes
        for name in attributeList:
            X[i, :] = fullList[name] 
            i = i + 1
        '''

        # parse csv file
        fullList = np.genfromtxt(filename, delimiter=',', skip_header=1, dtype='object', encoding=None)

        # decode from unicode and place in array
        decoded  = [[[ii.decode()] for ii in i] for i in fullList]
        toArray  = np.array(decoded)

        # slice up Y labels from X attributes
        Y = toArray[0:,0,0]
        X = toArray[0:,1:,0]
        X = X.transpose()

        
        #########################################
        return X,Y



