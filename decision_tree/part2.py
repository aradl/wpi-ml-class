import math
import sys
import numpy as np
from graphviz import Digraph
from collections import Counter

# bring in old code
import part1 as ID3


def load_dataset(filename = 'credit.txt'):
    
    # parse csv file
    fullList = np.genfromtxt(filename, skip_header=1, dtype='object', encoding=None)
    
    # get names
    names = np.genfromtxt(filename, dtype='object', encoding=None)
    names = names[0,0:]

    # decode from unicode and place in array
    decoded = [[i.decode()] for i in names]
    names   = np.array(decoded)
    names   = names.transpose()
    names   = names[0,:]
    names   = names[1:]

    # decode from unicode and place in array
    decoded  = [[[ii.decode()] for ii in i] for i in fullList]
    toArray  = np.array(decoded)

    # slice up Y labels from X attributes
    Y = toArray[0:,-1,0]
    X = toArray[0:,:-1,0]

    X = X.transpose()

    # remove name row
    X = np.delete(X, 0, 0)

    return X,Y, names

#
def train():
    X, Y, n = load_dataset()
    t = ID3.Tree.train(X, Y)

    return t, n

#
def tree_search(t, dot, edge, k, n):

    nodeName = k + '->' + str(n[t.i])
    myEdge   = nodeName + 'E'

    # is their a node above me
    if(edge != 0):
        dot.edge(str(edge), str(myEdge))

    # where in the tree are we
    if(not t.isleaf):
        # create node
        dot.node(myEdge, nodeName)
        print( k + '->' + n[t.i]) # DEBUG
        sys.stdout.write('\t')    # DEBUG

        for key in t.C: #DEBUG
            sys.stdout.write('-' + key + ' ')#DEBUG

        print(' ')#DEBUG
        sys.stdout.write('\t')#DEBUG
        for key in t.C:
            if(not t.C[key].isleaf): 
                print(' ')#DEBUG
                sys.stdout.write('\t')#DEBUG

            tree_search(t.C[key], dot, myEdge, key, n)

    else:
        # create node
        dot.node(myEdge, k + '->' + t.p)
        sys.stdout.write('|' + k + '-' + t.p) # DEBUG

#
def draw(t, n):
    dot = Digraph(comment='EquaLearn 2.0')

    # loop through tree
    edge = 0
    tree_search(t, dot, edge, 'root', n)
    print('')

    dot.render('tree_credit.gv', view=True)


# run
t, n = train()
draw(t, n)




