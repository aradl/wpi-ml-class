import os
import argparse
import csv
import numpy as np
import matplotlib.pyplot as plt
import scipy.io

from sklearn.svm import SVC
from sklearn.neighbors import KNeighborsClassifier
from sklearn.tree import DecisionTreeClassifier

from sklearn.preprocessing import LabelBinarizer
from sklearn.metrics import confusion_matrix
from sklearn.metrics import accuracy_score
from sklearn.utils.multiclass import unique_labels

from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, Flatten
from keras.layers import Conv1D, MaxPooling1D, LSTM

from keras.utils import plot_model
from keras import callbacks

#import pdb; pdb.set_trace() #DEBUG

# ignore TS warning - TODO compile from source
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

'''
    For help see:
        https://keras.io/getting-started/sequential-model-guide/
'''

class Classify():
    '''
        This class will host each classifier to test out against the
        PRN (pseudo random number) dataset where out goal is to pick out
        which generator the number set came from

        Each function will be a different method with the predicted values returned
    '''
    def __init__(self, X, Y):
        self.X = X
        self.Y = Y

    def split_time_windowed(self, window_size):
        # split dataset up into time slices
        packet_amt  = self.X['train'].shape[0]
        split_amt   = int(packet_amt / window_size)

        feature_amt = self.X['train'][0].shape[0]
        class_amt   = np.unique(self.Y['train']).shape[0]

        # we have to also do this for the test data as it could
        # be a different size
        t_packet_amt = self.X['test'].shape[0]
        t_split_amt  = int(t_packet_amt / window_size)

        # pre create arrays for everything
        x_train = np.zeros((split_amt, window_size, feature_amt))
        x_test  = np.zeros((t_split_amt, window_size, feature_amt))
        y_train = np.zeros(split_amt)

        # lets split up data and insure sequence length divides
        # into integer amounts for each label
        for l in range(1, class_amt+1):
            # find all values for the current label
            x_l_idx   = np.where(self.Y['train'] == l)
            t_x_l_idx = np.where(self.Y['test'] == l)

            # time segment by window_size
            p_x_set = x_l_idx[0].shape[0] / window_size
            if(np.mod(p_x_set, 1) != 0):
                print('ERROR: sequence length for train time slicing does not produce integer division')
                print('\t we get: {0}'.format(p_x_set))
            else:
                p_x_set = int(p_x_set) 

            # time segment by window_size
            t_p_x_set = t_x_l_idx[0].shape[0] / window_size
            if(np.mod(t_p_x_set, 1) != 0):
                print('ERROR: sequence length for test time slicing does not produce integer division')
                print('\t we get: {0}'.format(t_p_x_set))
            else:
                t_p_x_set = int(t_p_x_set) 

            # grab set of data from the current label
            x_set = self.X['train'][x_l_idx, :]

            x_set = np.reshape(x_set, (p_x_set, window_size, feature_amt))

            t_x_set = self.X['test'][t_x_l_idx, :]
            t_x_set = np.reshape(t_x_set, (t_p_x_set, window_size, feature_amt))

            # retain labels
            y_set = np.ones(p_x_set) * l

            # merge into master set 
            #(TODO option to randomize)
            m_idx_l = p_x_set * (l - 1)
            m_idx_h = (p_x_set * l)

            x_train[m_idx_l:m_idx_h, :, :] = x_set
            y_train[m_idx_l:m_idx_h]       = y_set

            t_m_idx_l = t_p_x_set * (l - 1)
            t_m_idx_h = (t_p_x_set * l)

            x_test[t_m_idx_l:t_m_idx_h, :, :] = t_x_set

        return x_train, x_test, y_train
        

    def SVM(self):
        clf = SVC(C=0.7, gamma='auto', decision_function_shape='ovr')

        print('training SVM ...')
        clf.fit(self.X['train'], self.Y['train']) 

        print('predicting SVM ...')
        self.y_pred = clf.predict(self.X['test'])

    def kNN(self):
        neigh = KNeighborsClassifier(n_neighbors=4)

        print('training kNN ...')
        neigh.fit(self.X['train'], self.Y['train']) 

        print('predicting kNN ...')
        self.y_pred = neigh.predict(self.X['test'])

    def decision_tree(self):
        clf = DecisionTreeClassifier(random_state=0, presort=True)

        print('training dtree ...')
        clf.fit(self.X['train'], self.Y['train']) 

        print('predicting dtree ...')
        self.y_pred = clf.predict(self.X['test'])

    def CNN(self):
        # get dataset information
        s_input  = self.X['train'][0].shape[0]
        s_output = np.unique(self.Y['train']).shape[0]

        # setup CNN parameters
        seq_length  = 10
        feature_amt = s_input
        class_amt   = s_output

        # time window data
        x_train, x_test, y_train = self.split_time_windowed(seq_length)

        # build CNN model
        #print(model.summary()) #DEBUG
        model = Sequential()
        model.add(Conv1D(256, 5, activation='relu', input_shape=(seq_length, feature_amt)))
        model.add(Conv1D(256, 5, activation='relu'))

        model.add(MaxPooling1D(2))
        model.add(Dropout(0.25))
         
        model.add(Flatten())
        model.add(Dense(128, activation='relu'))
        model.add(Dense(128, activation='relu'))
        model.add(Dropout(0.5))
        model.add(Dense(class_amt, activation='softmax'))

        model.compile(loss='categorical_crossentropy',
                        optimizer='adam',
                        metrics=['accuracy'])

        # create graph of network
        plot_model(model, to_file='cnn_model.png', show_shapes=True) 

        # convert labels to 'binary' representation for 
        # last layer classification
        enc     = LabelBinarizer()
        y_train = enc.fit_transform(y_train)

        print('training CNN ...')
        model.fit(x_train, y_train, 
                  batch_size=32, epochs=10, verbose=1)

        print('predicting CNN ...')
        labeled_y = model.predict_classes(x_test, batch_size=32)

        # labels from dataset start at 1 not 0
        labeled_y = labeled_y + 1

        # convert y to label each test entry
        t_y_pred  = np.ones((seq_length, 1)) * labeled_y[0]

        for i, x in enumerate(np.nditer(labeled_y)):
            # this is lazy
            if(i != 0):
                stacked  = np.ones((seq_length, 1)) * x
                t_y_pred = np.vstack((t_y_pred, stacked))

        self.y_pred = t_y_pred

    def LSTM(self):
        # get dataset information
        s_input  = self.X['train'][0].shape[0]
        s_output = np.unique(self.Y['train']).shape[0]

        # setup LSTM parameters
        seq_length  = 10
        feature_amt = s_input
        class_amt   = s_output

        # time window data
        x_train, x_test, y_train = self.split_time_windowed(seq_length)

        # expected input data shape: (batch_size, seq_length, feature_amt)
        model = Sequential()
        model.add(LSTM(256, return_sequences=True,
                    input_shape=(seq_length, feature_amt))) 
        #model.add(LSTM(128, return_sequences=True))
        model.add(LSTM(128))  
        model.add(Dense(80, activation='relu'))
        #model.add(Dropout(0.25))
        model.add(Dense(class_amt, activation='softmax'))

        model.compile(loss='categorical_crossentropy',
                        optimizer='adam',
                        metrics=['accuracy'])

        # create graph of network
        plot_model(model, to_file='lstm_model.png', show_shapes=True) 

        # convert labels to 'binary' representation for 
        # last layer classification
        enc     = LabelBinarizer()
        y_train = enc.fit_transform(y_train)

        callback = [callbacks.EarlyStopping(monitor='acc',min_delta=0,
                                    patience=5,
                                    verbose=1, mode='auto')
                    ]

        print('training LSTM...')
        model.fit(x_train, y_train,
                  batch_size=32, epochs=50, shuffle=True,
                  verbose=1, callbacks=callback)

        print('predicting LSTM...')
        labeled_y = model.predict_classes(x_test, batch_size=32)

        # labels from dataset start at 1 not 0
        labeled_y = labeled_y + 1

        # convert y to label each test entry
        t_y_pred  = np.ones((seq_length, 1)) * labeled_y[0]

        for i, x in enumerate(np.nditer(labeled_y)):
            # this is lazy
            if(i != 0):
                stacked  = np.ones((seq_length, 1)) * x
                t_y_pred = np.vstack((t_y_pred, stacked))

        self.y_pred = t_y_pred


'''
    Function modified from:
        https://scikit-learn.org/stable/auto_examples/model_selection/plot_confusion_matrix.html#sphx-glr-auto-examples-model-selection-plot-confusion-matrix-py
'''
def plot_confusion_matrix(y_true, y_pred,
                          normalize=True,
                          title=None,
                          cmap=plt.cm.Blues):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    if not title:
        if normalize:
            title = 'Normalized confusion matrix'
        else:
            title = 'Confusion matrix, without normalization'

    acc   = accuracy_score(y_true, y_pred)
    title = '{0} | Accuracy: {1}'.format(title, acc)

    # Compute confusion matrix
    cm = confusion_matrix(y_true, y_pred)
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')

    print('Accuracy: {0}'.format(acc))
    print(cm)

    # Only use the labels that appear in the data
    un = unique_labels(y_true, y_pred) - 1
    classes = un#classes[un]

    fig, ax = plt.subplots(figsize=(20, 20))
    im = ax.imshow(cm, interpolation='nearest', cmap=cmap)
    ax.figure.colorbar(im, ax=ax)
    # We want to show all ticks...
    ax.set(xticks=np.arange(cm.shape[1]),
           yticks=np.arange(cm.shape[0]),
           # ... and label them with the respective list entries
           xticklabels=classes, yticklabels=classes,
           title=title,
           ylabel='True label',
           xlabel='Predicted label')

    # Rotate the tick labels and set their alignment.
    plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
             rotation_mode="anchor")

    # Loop over data dimensions and create text annotations.
    fmt = '.2f' if normalize else 'd'
    thresh = ((cm.max() - cm.min()) / 2.) + cm.min()
    for i in range(cm.shape[0]):
        for j in range(cm.shape[1]):
            ax.text(j, i, format(cm[i, j], fmt),
                    ha="center", va="center",
                    color="white" if cm[i, j] > thresh else "black")
    fig.tight_layout()
    return ax

'''
    Pick and choose data sets and classifiers 
'''
def run(args):
    # setup dataset location
    dataset_dir = './dataset'

    f_test_x  = os.path.join(dataset_dir, 'Test_data_40classes_Tor_Browser.mat')
    f_test_y  = os.path.join(dataset_dir, 'Test_label_40classes_Tor_Browser.mat')
    f_train_x = os.path.join(dataset_dir, 'Training_data_40classes_Tor_Browser.mat')
    f_train_y = os.path.join(dataset_dir, 'Training_label_40classes_Tor_Browser.mat')

    title = 'Side-Channel '

    # read in dataset information
    print('loading data ...')
    test_x  = scipy.io.loadmat(f_test_x)
    test_y  = scipy.io.loadmat(f_test_y)
    train_x = scipy.io.loadmat(f_train_x)
    train_y = scipy.io.loadmat(f_train_y)

    # split up data and labels
    X = {}
    X['test']  = test_x['X_test']
    X['train'] = train_x['X_train3']

    Y = {}
    Y['test']  = test_y['Ytest'][0]
    Y['train'] = train_y['Ytrain'][0]

    # only use a fraction of the dataset 
    if(args.quick):
        frac = 0.25
        #TODO        

    # setup classifier
    clf = Classify(X, Y)

    # run selected algorithm
    if(args.svm):
        clf.SVM()
        title += 'SVM'

    elif(args.kNN):
        clf.kNN()
        title += 'kNN'

    elif(args.dtree):
        clf.decision_tree()
        title += 'decision tree'

    elif(args.cnn):
        clf.CNN()
        title += 'CNN'

    elif(args.lstm):
        clf.LSTM()
        title += 'LSTM'

    # setup output
    print('generating plot ...')
    plot_confusion_matrix(Y['test'], clf.y_pred, title='CM {0}'.format(title))
    title = title.replace(" ", "_")
    plt.savefig('CM_{0}.png'.format(title))

def main():     
    # parse command line arguments
    parser = argparse.ArgumentParser()

    # arguments 
    parser.add_argument('-s', '--svm',   action='store_true', 
                            help='Select support vector machine classifier type') 

    parser.add_argument('-k', '--kNN',   action='store_true', 
                            help='Select k-nearest neighboors classifier type') 

    parser.add_argument('-d', '--dtree', action='store_true', 
                            help='Select decision tree classifier type')  

    parser.add_argument('-c', '--cnn', action='store_true', 
                            help='Select convolutional classifier type')  

    parser.add_argument('-l', '--lstm', action='store_true', 
                            help='Select long short-term  memory classifier type')  

    parser.add_argument('-qq',  '--quick',  action='store_true', help='Reduce dataset amount (Debug)') 
    parser.add_argument('-a',  '--all',  action='store_true', 
                        help='Run everything (this should be your only argument) (Debug)') 

    args = parser.parse_args()

    if(args.all):
        # loop through datasets
        print('running everything ...')
        print('... (hang on tight)')
        # loop through classifiers
        for j in range(5):
            if j == 0:
                args.svm = True
            elif j == 1:
                args.svm = False
                args.kNN = True
            elif j == 2:
                args.kNN = False
                args.dtree = True
            elif j == 3:
                args.dtree = False
                args.cnn = True
            elif j == 4:
                args.cnn = False
                args.lstm = True

            # run selection
            print('part: {0}/5'.format(j+1))
            run(args)
        
    else:
        run(args)

if __name__ == '__main__':     
	main()
