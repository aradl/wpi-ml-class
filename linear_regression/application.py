import math
import numpy as np
from linear_regression import *
from sklearn.datasets import make_regression
# Note: please don't add any new package, you should solve this problem using only the packages above.
#-------------------------------------------------------------------------
'''
    Problem 2: Apply your Linear Regression
    In this problem, use your linear regression method implemented in problem 1 to do the prediction.
    Play with parameters alpha and number of epoch to make sure your test loss is smaller than 1e-2.
    Report your parameter, your train_loss and test_loss 
    Note: please don't use any existing package for linear regression problem, use your own version.
'''

#--------------------------

n_samples = 200
X,y = make_regression(n_samples= n_samples, n_features=4, random_state=1)
y = np.asmatrix(y).T
X = np.asmatrix(X)
Xtrain, Ytrain, Xtest, Ytest = X[::2], y[::2], X[1::2], y[1::2]

#########################################
## INSERT YOUR CODE HERE

# function list
'''
def compute_Phi(x,p):
def compute_yhat(Phi, w):
def compute_L(yhat,y):
def compute_dL_dw(y, yhat, Phi):
def update_w(w, dL_dw, alpha = 0.001):
def train(X, Y, alpha=0.001, n_epoch=100):
'''

# values I found to meet <1e-2 error
alpha = 0.1
epoch = 100

# train to get required test loss
w_trained = train(Xtrain, Ytrain, alpha, epoch)
y_hat     = compute_yhat(Xtrain, w_trained)
trained_l = compute_L(y_hat, Ytrain)
#
y_hat  = compute_yhat(Xtest, w_trained)
test_l = compute_L(y_hat, Ytest)

print('For alpha: {} and # of epoch: {}'.format(alpha, epoch))

print('    Trained Loss: {}'.format(trained_l))
print('    Test    Loss: {}'.format(test_l))





#########################################

